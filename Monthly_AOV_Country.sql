-- Monthly AOV by Country

-- find cost of every order
WITH t1 as 
(
    SELECT InvoiceNo, 
    toStartOfMonth(InvoiceDate) AS Month, 
    SUM(Quantity * UnitPrice) AS Cost, 
    Country
    
    FROM retail
    WHERE  Description != 'Manual'       -- filter data on positions removed from the order
    and  Quantity > 0                    -- filter data on returned goods 
    and Month != '2011-12-01'            -- filter data for the last month (incomplete)
    and Country  ==  '{{ Country }}'     -- create a parameter for the filter
    GROUP BY Country, InvoiceNo, Month
)

SELECT Country, Month,
       AVG(Cost) AS AOV

FROM t1
GROUP BY Country, Month
Order by Month ASC, AOV DESC
