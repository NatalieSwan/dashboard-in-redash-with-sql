-- Average Order Value (AOV) - all countries

-- find sum of every order
WITH t1 as 
(
    SELECT InvoiceNo,
    SUM(Quantity * UnitPrice) AS TotalCost, 
    Country
    FROM retail
    WHERE  Description != 'Manual' 
    and  Quantity > 0
    and toStartOfMonth(InvoiceDate) != '2011-12-01'
    GROUP BY Country, InvoiceNo
)

SELECT Country, 
Round(AVG(TotalCost), 2) AS AOV
FROM t1
GROUP BY Country
Order by AOV 
