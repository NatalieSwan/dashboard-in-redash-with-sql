-- Average Order Value (AOV) & Num. Orders exclud. UK

WITH t1 as 
(
    SELECT InvoiceNo,
    toStartOfMonth(InvoiceDate) AS Month, 
    SUM(Quantity * UnitPrice) AS Cost
    
    FROM retail
    WHERE  Description != 'Manual'      -- filter data on positions removed from the order
    and  Quantity > 0                   -- filter data on returned goods
    and Month != '2011-12-01'           -- filter data for the last month (incomplete)
    and Country != 'United Kingdom'     -- filter data by UK
    GROUP BY InvoiceNo, Month 
)

SELECT  Month,
AVG(Cost) AS AOV,
COUNT(DISTINCT InvoiceNo) AS NumOrders
FROM t1
GROUP BY Month 
Order by Month ASC
