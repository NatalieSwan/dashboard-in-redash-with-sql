-- TOP 100 Products (exclud. UK)

SELECT Description, SUM(Quantity) as num_merch
from retail 
WHERE Description != 'Manual'                              -- filter data on positions removed from the order
        and  Quantity > 0                                   -- filter data on returned goods   
        and toStartOfMonth(InvoiceDate)  != '2011-12-01'   -- filter data for the last month (incomplete)
        and Country != 'United Kingdom'                    -- filter data by UK
Group by Description
Order by num_merch DESC
limit 100
