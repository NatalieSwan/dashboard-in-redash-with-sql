-- Avg Revenue Per User (ARPU) & Monthly Active Users (MAU) exclud. UK

WITH t1 as 
(
    SELECT CustomerID,
           toStartOfMonth(InvoiceDate) AS Month, 
           SUM(Quantity * UnitPrice) AS RevenuePerCustomer
    
    FROM retail
    WHERE  Description != 'Manual'           -- filter data on positions removed from the order
           and  Quantity > 0                 -- filter data on returned goods
           and Month != '2011-12-01'         -- filter data for the last month (incomplete)
           and Country != 'United Kingdom'   -- filter data by UK
    GROUP BY Month, CustomerID 
)

SELECT  Month,
        AVG(RevenuePerCustomer) AS ARPU,
        COUNT(DISTINCT CustomerID) AS MAU

FROM t1
GROUP BY Month 
Order by Month ASC
