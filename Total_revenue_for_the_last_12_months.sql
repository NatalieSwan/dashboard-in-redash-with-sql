--Total revenue for the last 12 months (Dec.2010 - Nov.2011)

SELECT toStartOfMonth(InvoiceDate) AS Month, 
    SUM(Quantity * UnitPrice) AS Revenue,
    SUM(Quantity) AS SalesVolume
FROM retail
WHERE Description != 'Manual'   -- filter data on positions removed from the order
    and  Quantity > 0           -- filter data on returned goods
    and Month != '2011-12-01'   -- filter data for the last month (incomplete)
    
GROUP BY Month
ORDER BY Month
